﻿using DealerOn.Model;

namespace Services.ViewModels
{
    public class GridInputViewModel
    {
        public int RoverCount { get; set; }
        public int InitialX { get; set; }
        public int InitialY { get; set; }
        public string InitialOrientation { get; set; }
        public int MaxHorizontalSize { get; set; }
        public int MaxVerticalSize { get; set; }
    }
}
