﻿using System;
namespace Services.ViewModels
{
    public class PointOutputViewModel
    {
        public int X { get; set; }
        public int Y { get; set; }
        public string Orientation { get; set; }
    }
}
