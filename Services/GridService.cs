﻿using System;
using DealerOn.Model;
using Services.ViewModels;

namespace Services
{
    public class GridService : IGridService
    {
        public Grid Grid { get; private set; }

        public void CreateGrid(int roverCount, Point initialPoint, Tuple<int, int> size)
        {
            this.Grid = new Grid(roverCount, initialPoint, size.Item1, size.Item2);
        }

        public PointOutputViewModel CreateGrid(GridInputViewModel gridInputViewModel)
        {
            Enum.TryParse(gridInputViewModel.InitialOrientation, out Orientation initialOrientation);
            Point point = new Point(gridInputViewModel.InitialX, gridInputViewModel.InitialY,
                initialOrientation);

            this.Grid = new Grid(gridInputViewModel.RoverCount, point, gridInputViewModel.MaxHorizontalSize, gridInputViewModel.MaxVerticalSize);
            return CreatePointOutputViewModel(point);
        }

        public PointOutputViewModel MoveRovers(string instructions)
        {
            if (this.Grid == null)
                throw new InvalidOperationException("Cannot move rovers without setting the grid.");

            this.Grid.LoadInstructions(instructions);
            return CreatePointOutputViewModel(this.Grid.ExecuteInstructions());
        }

        private static PointOutputViewModel CreatePointOutputViewModel(Point finalPoint)
        {
            PointOutputViewModel pointOutputViewModel = new PointOutputViewModel();
            pointOutputViewModel.X = finalPoint.X;
            pointOutputViewModel.Y = finalPoint.Y;
            pointOutputViewModel.Orientation = finalPoint.Orientation.ToString();
            return pointOutputViewModel;
        }
    }
}
