﻿using System;
using DealerOn.Model;
using Services.ViewModels;

namespace Services
{
    public interface IGridService
    {
        Grid Grid { get; }
        PointOutputViewModel CreateGrid(GridInputViewModel gridInputViewModel);
        PointOutputViewModel MoveRovers(string instructions);
    }
}
