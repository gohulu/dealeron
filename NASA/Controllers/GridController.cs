﻿using Microsoft.AspNetCore.Mvc;
using Services;
using Services.ViewModels;

namespace DealerOn.Controller
{
    [ApiController]
    [Route("[controller]")]
    public class GridController : ControllerBase
    {
        #region Fields
        private IGridService gridService;
        #endregion

        #region Constructor
        public GridController(IGridService gridService)
        {
            this.gridService = gridService;
        }
        #endregion

        #region Public interface
        [HttpPost("create")]
        public ActionResult<PointOutputViewModel> CreateGrid([FromBody] GridInputViewModel gridInputViewModel)
        {
            return Ok(this.gridService.CreateGrid(gridInputViewModel));
        }

        [HttpPost("rovers/move")]
        public ActionResult<PointOutputViewModel> MoveRovers([FromBody] string instructions)
        {
            return Ok(this.gridService.MoveRovers(instructions));
        }
        #endregion
    }
}
