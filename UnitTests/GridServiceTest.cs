﻿using NUnit.Framework;
using Services;
using Services.ViewModels;

namespace UnitTests
{
    [TestFixture]
    public class GridServiceTest
    {
        [TestCase]
        public void TestCase1()
        {
            GridInputViewModel gridInputViewModel = new GridInputViewModel();
            gridInputViewModel.RoverCount = 5;
            gridInputViewModel.InitialX = 1;
            gridInputViewModel.InitialY = 2;
            gridInputViewModel.InitialOrientation = "N";
            gridInputViewModel.MaxHorizontalSize = 5;
            gridInputViewModel.MaxVerticalSize = 5;

            IGridService gridService = new GridService();
            gridService.CreateGrid(gridInputViewModel);
            var result = gridService.MoveRovers("LMLMLMLMM");

            Assert.AreEqual(result, new PointOutputViewModel() { X = 1, Y = 2, Orientation = "N" });
        }

        [TestCase]
        public void TestCase2()
        {
            GridInputViewModel gridInputViewModel = new GridInputViewModel();
            gridInputViewModel.RoverCount = 5;
            gridInputViewModel.InitialX = 3;
            gridInputViewModel.InitialY = 3;
            gridInputViewModel.InitialOrientation = "E";
            gridInputViewModel.MaxHorizontalSize = 5;
            gridInputViewModel.MaxVerticalSize = 5;

            IGridService gridService = new GridService();
            gridService.CreateGrid(gridInputViewModel);
            var result = gridService.MoveRovers("MMRMMRMRRM");

            Assert.AreEqual(result, new PointOutputViewModel() { X = 5, Y = 1, Orientation = "E" });
        }
    }
}
