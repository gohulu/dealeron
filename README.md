**. . . include a brief explanation of your design and assumptions along with your code.**

The idea was to use the MVC pattern to represent the functionality of the software. At the end a two layer architecture was used which pointed at the end to the model which contained the models for the grid and the rovers. The is a Grid Object which contains the rover this one is a Singleton object.

The software is a backend API which contains two endpoints:

Grid/create

Used to create the grid in which the rovers exist. An example of the expected input for the body of this endpoint is the following:

{
  "roverCount": 5,
  "initialX": 1,
  "initialY": 3,
  "initialOrientation": "N",
  "maxHorizontalSize": 5,
  "maxVerticalSize": 5
}

Grid/rovers/move

Used to add the instructions that will move the rovers.

An example of the expected input for this endpoint is the following:

"LMLMLMLMM"

**. . . compile and run without us needing to do anything more than a package restore.**

Just need to do a git pull and execute the application as was mentioned in the previous point.

**. . . give the correct answer. Your output should match our expected outputs as closely as possible. Showing us unit tests are a plus!**
 
There are unit tests in the Unit Test folder.
