﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DealerOn.Model
{
    public class Grid
    {
        #region Fields

        #endregion

        #region Constructor
        public Grid(int roversCount, Point initialRoverPosition, int maxHorizontalPosition, int maxVerticalPosition)
        {
            this.Rovers = Enumerable.Range(1, roversCount)
                .Select(p => { return new Point(initialRoverPosition.X, initialRoverPosition.Y, initialRoverPosition.Orientation); }).ToList();
            this.MaxHorizontalPosition = maxHorizontalPosition;
            this.MaxVerticalPosition = maxVerticalPosition;
            this.Instructions = new List<Instruction>();
        }
        #endregion

        #region Public Interface
        public int MaxHorizontalPosition { get; private set; }
        public int MaxVerticalPosition { get; private set; }
        public IList<Point> Rovers { get; private set; }
        public IList<Instruction> Instructions { get; private set; }

        public void LoadInstructions(string instructions)
        {
            var instructionsArray = instructions.ToCharArray();
            int arrayLength = instructionsArray.Count();

            for (int i = 0; i < arrayLength; i++)
            {
                Enum.TryParse(instructionsArray[i].ToString(), out Instruction instruction);
                this.Instructions.Add(instruction);
            }
        }

        public Point ExecuteInstructions()
        {
            var newInstructions = this.Instructions;
            newInstructions.ToList().ForEach(instruction =>
            {
                Rovers.ToList().ForEach(rover =>
                {
                    rover.SetOrientation(instruction);
                    rover.Move(instruction);
                });
                this.Instructions = this.Instructions.Skip(1).ToList();
            });

            return Rovers[0];
        }
        #endregion

        #region Private Helpers
        
        #endregion
    }
}
