﻿
using System.Collections.Generic;

namespace DealerOn.Model
{
    public class Point
    {
        #region Fields
        private Dictionary<Orientation, Orientation> leftNextOrientations;
        private Dictionary<Orientation, Orientation> rightNextOrientiations;
        #endregion

        #region Constructor
        public Point(int x, int y, Orientation orientation)
        {
            CreatLeftNextOrientationsDictionary();
            CreateRightNextOrientationsDictionary();

            this.X = x;
            this.Y = y;
            this.Orientation = orientation;
        }
        #endregion

        #region Public Interface
        public int X { get; private set; }
        public int Y { get; private set; }
        public Orientation Orientation { get; private set; }

        public void Move(Instruction instruction)
        {
            if (instruction == Instruction.L || instruction == Instruction.R) return;

            if (this.Orientation == Orientation.N) this.Y++;
            if (this.Orientation == Orientation.W) this.X--;
            if (this.Orientation == Orientation.S) this.Y--;
            if (this.Orientation == Orientation.E) this.X++;
        }

        public void SetOrientation(Instruction instruction)
        {
            if (instruction == Instruction.M) return;

            this.Orientation = instruction == Instruction.L
                ? this.leftNextOrientations[this.Orientation]
                : this.rightNextOrientiations[this.Orientation];
        }

        public override string ToString()
        {
            return $"X: {this.X}, Y: {this.Y}, Orientation: {this.Orientation}";
        }
        #endregion

        #region Private Helpers

        private void CreatLeftNextOrientationsDictionary()
        {
            this.leftNextOrientations = new Dictionary<Orientation, Orientation>();
            this.leftNextOrientations.Add(Orientation.N, Orientation.W);
            this.leftNextOrientations.Add(Orientation.W, Orientation.S);
            this.leftNextOrientations.Add(Orientation.S, Orientation.E);
            this.leftNextOrientations.Add(Orientation.E, Orientation.N);
        }

        private void CreateRightNextOrientationsDictionary()
        {
            this.rightNextOrientiations = new Dictionary<Orientation, Orientation>();
            this.rightNextOrientiations.Add(Orientation.N, Orientation.E);
            this.rightNextOrientiations.Add(Orientation.E, Orientation.S);
            this.rightNextOrientiations.Add(Orientation.S, Orientation.W);
            this.rightNextOrientiations.Add(Orientation.W, Orientation.N);
        }
        #endregion
    }
}
